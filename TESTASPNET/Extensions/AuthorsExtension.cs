﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TESTASPNET.Models;

namespace TESTASPNET.Extensions
{
    public static class AuthorsExtension
    {
        public static string GetFullName(this AuthorDto author)
        {
            return string.Format("{0} {1}", author.FirstName, author.LastName);
        }
    }
}