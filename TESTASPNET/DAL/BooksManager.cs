﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TESTASPNET.Models;
using System.Data.Entity;

namespace TESTASPNET.DAL
{
    public static class BooksManager
    {
        public static List<BookDto> GetBooks()
        {
            List<BookDto> result = new List<BookDto>();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                // List<Book> books = db.Database.SqlQuery<Book>("StoredProcedures").ToList();
                //result = books.Select(x=>x.ToDto()).ToList();
                result = db.Books.Include(x => x.Authors).ToList().Select(x => x.ToDto()).ToList();
            }

            return result;
        }
    }
}