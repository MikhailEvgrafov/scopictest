﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TESTASPNET.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime PublishedOn { get; set; }
        public List<Author> Authors { get; set; }

        public BookDto ToDto()
        {
            BookDto result = new BookDto();
            result.Id = Id;
            result.Title = Title;
            result.PublishedOn = PublishedOn;
            result.Authors = new List<AuthorDto>();

            if (Authors != null)
                result.Authors = Authors.Select(x => x.ToDto()).ToList();

            return result;
        }
    }
}