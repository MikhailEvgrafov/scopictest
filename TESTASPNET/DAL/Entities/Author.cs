﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TESTASPNET.Models
{
    public class Author
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public AuthorDto ToDto()
        {
            AuthorDto result = new AuthorDto();
            result.Id = Id;
            result.FirstName = FirstName;
            result.LastName = LastName;

            return result;
        }
    }
}