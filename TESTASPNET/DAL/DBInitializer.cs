﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using TESTASPNET.Models;

namespace TESTASPNET.DAL
{
    public class DBInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            if (context.Authors.Count() == 0)
            {
                context.Authors.AddRange(new List<Author>()
                {
                    new Author() { Id = 1, FirstName = "John", LastName = "Malkovich" },
                    new Author() { Id = 2, FirstName = "Madlen", LastName = "Ditrich" },
                    new Author() { Id = 3, FirstName = "Mark", LastName = "Bedford" },
                    new Author() { Id = 4, FirstName = "Ikar", LastName = "Select" }
                }
                );

                context.SaveChanges();

                context.Books.AddRange(new List<Book>()
                {
                       new Book()
                       {
                           Id = 1,
                           Authors = new List<Author>() { context.Authors.Find(1), context.Authors.Find(2) },
                           PublishedOn = new DateTime(1967, 1, 2),
                           Title = "Wing of change"
                       },
                       new Book()
                       {
                          Id = 2,
                          Authors = new List<Author>() { context.Authors.Find(1) },
                          PublishedOn = new DateTime(1977, 2, 2),
                          Title = "Way of explorer"
                      },
                      new Book()
                      {
                          Id = 3,
                          Authors = new List<Author>() { context.Authors.Find(2),context.Authors.Find(3) },
                          PublishedOn = new DateTime(1987, 2, 2),
                          Title = "Way of explorer 2"
                      },
                      new Book()
                      {
                          Id = 4,
                          Authors = new List<Author>() { context.Authors.Find(3) },
                          PublishedOn = new DateTime(1937, 2, 2),
                          Title = "Conqueror"
                      },
                     new Book()
                     {
                         Id = 5,
                         Authors = new List<Author>() { context.Authors.Find(4),context.Authors.Find(1),context.Authors.Find(3) },
                         PublishedOn = new DateTime(1937, 4, 2),
                         Title = "Last way"
                     },
                     new Book()
                     {
                         Id = 5,
                         Authors = new List<Author>() { context.Authors.Find(4) },
                         PublishedOn = new DateTime(1937, 4, 2),
                         Title = "Move on"
                     }
                }
            );

                context.SaveChanges();

               
            }

            base.Seed(context);
        }
    }

}