﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TESTASPNET.Models
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime PublishedOn { get; set; }
        public List<AuthorDto> Authors { get; set; }
    }
}